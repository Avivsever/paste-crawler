import logging

logger = logging.getLogger("Pastebin Crawler Logger")
logger.setLevel("DEBUG")

stream_handler = logging.StreamHandler()

stream_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stream_handler.setFormatter(stream_format)

logger.addHandler(stream_handler)
