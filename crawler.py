import requests
from bs4 import BeautifulSoup
from tinydb import TinyDB, Query

import consts
from paste_parser import PasteParser


class Crawler:
    def __init__(self):
        self.db = TinyDB(consts.DB_FILE_PATH)

    def __extract_username(self, soup: BeautifulSoup) -> str:
        return soup.find("div", attrs={"class": "username"}).find("a").text

    def __extract_date(self, soup: BeautifulSoup) -> str:
        return soup.find("div", attrs={"class": "date"}).find("span")["title"]

    def __extract_title(self, soup: BeautifulSoup) -> str:
        return soup.find("div", attrs={"class": "info-top"}).text

    def __extract_content(self, soup: BeautifulSoup):
        return soup.find("textarea", attrs={"class": "textarea"}).text

    def get_paste(self, paste_url: str) -> dict:
        url = f"{consts.PASTEBIN_URL}{paste_url}"
        page = requests.get(url)
        soup = BeautifulSoup(page.content, "html.parser")

        username = self.__extract_username(soup)
        date = self.__extract_date(soup)
        title = self.__extract_title(soup)
        content = self.__extract_content(soup)

        paste = dict()
        paste["url"] = paste_url
        paste["title"] = PasteParser.parse_title(title)
        paste["author"] = PasteParser.parse_author(username)
        paste["date"] = PasteParser.parse_date(date)
        paste["content"] = PasteParser.parse_content(content)
        return paste

    def crawl(self):
        page = requests.get(consts.ARCHIVE_URL)
        soup = BeautifulSoup(page.content, "html.parser")

        table = soup.body.find('table', attrs={"class": "maintable"})
        paste_urls = [row.find('a')['href'] for row in table.findAll('tr') if row.td]
        for paste_url in paste_urls:
            paste_query = Query()
            if not self.db.search(paste_query.url == paste_url):
                paste = self.get_paste(paste_url)
                self.db.insert(paste)
