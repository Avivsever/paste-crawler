from datetime import timezone

from dateutil import parser

import consts


class PasteParser:

    @staticmethod
    def parse_author(author: str) -> str:
        if author in consts.EMPTY_USERNAMES:
            return str()
        return author

    @staticmethod
    def parse_title(title: str) -> str:
        if title in consts.EMPTY_TITLES:
            return str()
        return title

    @staticmethod
    def parse_content(content: str) -> str:
        return content.rstrip()

    @staticmethod
    def parse_date(date) -> str:
        # This returns str and not date because tinyDB works with Json
        timezoned_date = parser.parse(date,
                                      tzinfos={"CDT": -5 * 3600})  # This is an American timezone and is not supported
        utc_date = timezoned_date.astimezone(timezone.utc)
        return str(utc_date)
