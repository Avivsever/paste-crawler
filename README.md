Welcome to Pastebin.com crawler

Current directory should be `PasteCrawler`

To run the crawler run the script `run.py`

To run the crawler on docker run these commands:
* docker build -t python-crawler
* docker run --name pastebin_crawler python-crawler

That's it. Enjoy :)